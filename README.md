fedi-share
==========

A landing page to ask a user for their Fediverse instance before sending them there.

Built in mind to be privacy-friendly, no data is stored on the server, if you ask
it to remember your instance's domain it'll be stored on your browser.

Everything is contained in a single HTML file to make it easy to audit. You're also
welcome to copy it and host your own instance, pursuant to the license.

The main instance is deployed at <https://fedi-share.toolforge.org/>.

## URL format

Generate a link like `https://fedi-share.toolforge.org/?text=Your post here`

If it contains newlines or other weird characters, you can also JSON-encode the string
and pass it in via the `?json=` parameter. For example, `?json=%22foo%5Cnbar%5Cn%23baz%22`.

For integration with Wikimedia wikis, you can use [Module:Fedi-share](https://commons.wikimedia.org/wiki/Module:Fedi-share)
to generate links.

## Future ideas
Can we read the text out of the fragment instead of using a query parameter? That would prevent
the server from even knowing the text.

Would also be nice if it was prettier, etc.

## Motivation
We needed to replace the Twitter share buttons on the [Commons Picture of the Year contest](https://commons.wikimedia.org/wiki/Commons:Picture_of_the_Year).

## License
fedi-share is available under the AGPL v3 or any later version, see COPYING for more details.
